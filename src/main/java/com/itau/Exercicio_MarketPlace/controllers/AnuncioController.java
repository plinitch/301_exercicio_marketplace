package com.itau.Exercicio_MarketPlace.controllers;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.Exercicio_MarketPlace.models.Anuncio;
import com.itau.Exercicio_MarketPlace.models.Usuario;
import com.itau.Exercicio_MarketPlace.repositories.AnuncioRepository;
import com.itau.Exercicio_MarketPlace.repositories.UsuarioRepository;
import com.itau.Exercicio_MarketPlace.services.JWTService;

@RestController
@RequestMapping("/anuncio")
public class AnuncioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	JWTService jwtService;
	
	@Autowired
	AnuncioRepository anuncioRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Anuncio> criar(@RequestHeader (value="Authorization") String authorization, @RequestBody Anuncio anuncio) {
		
		authorization = authorization.replace("Bearer ", "");
		
		String username = jwtService.validarToken(authorization);
		
		if(username == null) {
			return ResponseEntity.status(403).build();
		}
		
		Optional<Usuario> usuarioOptional = usuarioRepository.findByUsername(username);
		
		if(!usuarioOptional.isPresent()) {
			return ResponseEntity.status(403).build();
		}
		
		anuncio.setId(UUID.randomUUID());
		Usuario user = usuarioOptional.get();
		
		anuncio.setUsername(user.getUsername());

		anuncio.setUsuario(user);
		
		return ResponseEntity.status(HttpStatus.OK).body(anuncioRepository.save(anuncio));
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscar() {
		
		Iterable<Anuncio> a = anuncioRepository.findAll();
		
		a.forEach(anuncio -> {
			anuncio.setUsuario(usuarioRepository.findByUsername(anuncio.getUsername()).get());
			anuncio.getUsuario().setSenha(null);
		});
		
		return a;
	}
}
