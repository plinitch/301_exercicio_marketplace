package com.itau.Exercicio_MarketPlace.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.Exercicio_MarketPlace.models.Anuncio;
import com.itau.Exercicio_MarketPlace.models.Usuario;
import com.itau.Exercicio_MarketPlace.repositories.UsuarioRepository;
import com.itau.Exercicio_MarketPlace.services.JWTService;
import com.itau.Exercicio_MarketPlace.services.PasswordService;

@RestController
public class LoginController {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	JWTService jwtService;
	
	@Autowired
	PasswordService passwordService;

	@RequestMapping("/login")
	public ResponseEntity<?> fazerLogin(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBancoOptional = usuarioRepository.findByUsername(usuario.getUsername());
		if(!usuarioBancoOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		Usuario usuarioBanco = usuarioBancoOptional.get(); 
		
		if(passwordService.verificarHash(usuario.getSenha(), usuarioBanco.getSenha())) {
			String token = jwtService.gerarToken(usuarioBanco.getUsername());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			
			return new ResponseEntity<Usuario>(usuarioBanco, headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}


}
