package com.itau.Exercicio_MarketPlace.controllers;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.Exercicio_MarketPlace.models.Oferta;
import com.itau.Exercicio_MarketPlace.repositories.OfertaRepository;

@RestController
@RequestMapping("/oferta")
public class OfertaController {
	@Autowired
	OfertaRepository ofertaRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public Oferta criar(@RequestBody Oferta oferta) {
		oferta.setId(UUID.randomUUID());
		return ofertaRepository.save(oferta);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscar() {
		return ofertaRepository.findAll();
	}
}
