package com.itau.Exercicio_MarketPlace.controllers;


import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.Exercicio_MarketPlace.models.Usuario;
import com.itau.Exercicio_MarketPlace.repositories.UsuarioRepository;
import com.itau.Exercicio_MarketPlace.services.PasswordService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@RequestMapping(method=RequestMethod.POST)
	public Usuario criar(@Valid @RequestBody Usuario usuario) {

		String hash = passwordService.gerarHash(usuario.getSenha());
		usuario.setSenha(hash);
		
		usuarioRepository.save(usuario);
		
		return usuario;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscar() {
		return usuarioRepository.findAll();
	}

}
