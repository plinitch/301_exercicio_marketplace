package com.itau.Exercicio_MarketPlace.models;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.format.annotation.NumberFormat;

@Table
public class Anuncio {

	@PrimaryKey
	private UUID id;
	
	

	@NotNull
	private String titulo;
	
	@NotNull
	private String descricao;
	
	@NotNull
	@NumberFormat
	private double preco;
	
	@NotNull
	private String data;
	
	@NotNull
	private String username;
	
	@Transient
	private Usuario usuario;
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	

	
}
