package com.itau.Exercicio_MarketPlace.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.Exercicio_MarketPlace.models.Anuncio;

public interface AnuncioRepository extends CrudRepository<Anuncio, UUID>{

}
