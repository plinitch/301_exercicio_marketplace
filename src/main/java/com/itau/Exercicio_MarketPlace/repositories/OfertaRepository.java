package com.itau.Exercicio_MarketPlace.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.Exercicio_MarketPlace.models.Oferta;

public interface OfertaRepository extends CrudRepository<Oferta, UUID> {

}
