package com.itau.Exercicio_MarketPlace.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.data.repository.CrudRepository;

import com.itau.Exercicio_MarketPlace.models.Usuario;

@Table
public interface UsuarioRepository extends CrudRepository<Usuario, UUID> {

	public Optional<Usuario> findByUsername(String username);
	
}
